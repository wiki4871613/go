package main

import (
	api "hexarch/internal/api/factory"
	db "hexarch/internal/database/factory"
	"os"

	"hexarch/internal/core"

	"log"
)

func main() {

	tl, _ := db.NewTransactionLogger(os.Getenv("TLOG_TYPE"))

	store := core.NewKeyValueStore().WithTransactionLogger(tl)
	store.Restore()

	fe, _ := api.NewApi(os.Getenv("TLOG_API"))

	log.Fatal(fe.Start(store))
}
