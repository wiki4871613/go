package gRPC

import (
	"fmt"
	"hexarch/internal/core"
	pb "hexarch/internal/gRPC"
	"net"

	"google.golang.org/grpc"
)

// * Структура server используется в реализации KeyValueServer. Она ДОЛЖНА
// * встраивать сгенерированную структуру pb.UnimplementedKeyValueServer
type ApiGRPC struct {
	pb.UnimplementedKeyValueServer
	store *core.KeyValueStore
}

func (a *ApiGRPC) Start(store *core.KeyValueStore) error {

	a.store = store
	//* Создать сервер gRPC и зарегистрировать в нем наш KeyValueServer
	grpc.NewServer()
	s := grpc.NewServer()
	pb.RegisterKeyValueServer(s, a)

	//* Открыть порт 50051 для приема сообщений
	lis, err := net.Listen("tcp", ":50051")
	if err != nil {
		return fmt.Errorf("failed to listen: %v", err)
	}

	//* Начать цикл приема и обработку запросов
	if err := s.Serve(lis); err != nil {
		return fmt.Errorf("failed to serve: %v", err)
	}

	return nil
}
