package gRPC

import (
	"context"
	pb "hexarch/internal/gRPC"
	"log"
)

func (a *ApiGRPC) Get(ctx context.Context, r *pb.GetRequest) (*pb.GetResponse, error) {
	log.Printf("Received GET key=%v", r.Key)
	//* Локальная функция Get была реализована в главе 5
	value, err := a.store.Get(r.Key)
	//* Вернуть полученные от GetResponse указатель и признак ошибки
	return &pb.GetResponse{Value: value}, err
}

func (a *ApiGRPC) Put(ctx context.Context, r *pb.PutRequest) (*pb.PutResponse, error) {
	log.Printf("Received PUT key=%v value=%v", r.Key, r.Value)
	//* Локальная функция Get была реализована в главе 5

	err := a.store.Put(r.Key, r.Value)
	if err != nil {
		return nil, err
	}

	//* Вернуть полученные от GetResponse указатель и признак ошибки
	return &pb.PutResponse{}, err
}
