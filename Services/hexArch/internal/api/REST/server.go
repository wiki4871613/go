package REST

import (
	"hexarch/internal/core"
	"net/http"

	"github.com/gorilla/mux"
)

type ApiRest struct {
	store *core.KeyValueStore
}

func (a *ApiRest) Start(store *core.KeyValueStore) error {
	a.store = store

	r := mux.NewRouter()

	r.Use(a.loggingMiddleware)

	r.HandleFunc("/v1/{key}", a.PutHandler).Methods("PUT")       //* curl -X PUT -d 'Hello, key-value store!' -v http://localhost:8080/v1/key-a
	r.HandleFunc("/v1/{key}", a.GetHandler).Methods("GET")       //* curl -v http://localhost:8080/v1/key-a
	r.HandleFunc("/v1/{key}", a.DeleteHandler).Methods("DELETE") //* curl -v -X DELETE http://localhost:8080/v1/key-a

	r.HandleFunc("/v1", a.notAllowedHandler)
	r.HandleFunc("/v1/{key}", a.notAllowedHandler)

	return http.ListenAndServe(":8080", r)
}
