package factory

import (
	"fmt"
	"hexarch/internal/api/REST"
	"hexarch/internal/api/gRPC"
	"hexarch/internal/core"
)

type Api interface {
	Start(kv *core.KeyValueStore) error
}

type zeroApi struct{}

func (a zeroApi) Start(kv *core.KeyValueStore) error {
	return nil
}

func NewApi(s string) (Api, error) {
	switch s {
	case "zero":
		return zeroApi{}, nil

	case "rest":
		return &REST.ApiRest{}, nil

	case "grpc":
		return &gRPC.ApiGRPC{}, nil

	default:
		return nil, fmt.Errorf("no such frontend %s", s)
	}
}
