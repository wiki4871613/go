package factory

import (
	"hexarch/internal/core"
	l "hexarch/internal/database/files"
	p "hexarch/internal/database/pg"
	t "hexarch/internal/database/test"

	"fmt"
)

func NewTransactionLogger(s string) (core.TransactionLogger, error) {
	switch s {
	case "test":
		return t.NewTestTransactionLogger()

	case "file":
		return l.NewFileTransactionLogger("./transactions.txt")

	case "postgres":
		params := p.PostgresDbParams{
			Host: "localhost", DbName: "kvs",
			User: "test", Password: "hunter2",
		}
		return p.NewPostgresTransactionLogger(params)

	default:
		return nil, fmt.Errorf("no such transaction logger %s", s)
	}
}
