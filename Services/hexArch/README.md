# hexArch

## Содержание

- [Описание](#about)
- [Начало работы](#getting_started)
- [Использование](#usage)

## Описание <a name = "about"></a>

Пример простого приложения, реализующий подход гексагональный архитектуры.
1. приложение представляет из себя хранилище пар ключ/значение (события) и работает с ассоциативным массивом в памяти.
2. доступ к этому приложению можно получить через интерфейс REST или gRPC.
3. новые события записываются postgres или локальный файл и читаются обратно после запуска приложения.

## Начало работы <a name = "getting_started"></a>

- [Ссылка на телеграм канал](https://t.me/ITLodgeBack/6)
- [Ссылка на подробный дайджест](https://telegra.ph/Geksagonalnaya-arhitektura-04-15)



## Использование <a name = "usage"></a>

### запуск сервера
```
export TLOG_TYPE=<file> or <postgres> // логирование в файл или postgres
export TLOG_API=<rest> or <grpc> // подключение внешнего API

go mod tidy
go run cmd/server/main.go

```
### использование REST

```
curl -X PUT -d 'Hello, key-value store!' -v http://localhost:8080/v1/key-a
curl -v http://localhost:8080/v1/key-a
curl -v -X DELETE http://localhost:8080/v1/key-a
```
### использование gRPC

```
go run cmd/client/main.go put foo bar
go run cmd/client/main.go get foo
```
