package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gorilla/mux"
)

var throttled = Throttle(getHostname, 10, 1, time.Microsecond)

func getHostname(ctx context.Context) (string, error) {
	if ctx.Err() != nil {
		return "", ctx.Err()
	}
	return os.Hostname()
}

func throttledHandler(w http.ResponseWriter, r *http.Request) {

	ok, hostname, err := throttled(r.Context(), strings.Split(r.RemoteAddr, ":")[0])

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if !ok {
		http.Error(w, "Too many requests", http.StatusTooManyRequests)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(hostname))
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/hostname", throttledHandler)
	log.Fatal(http.ListenAndServe(":8080", r))
}

// * функция контракт
type Effector func(context.Context) (string, error)

// * ф-н которая возвращается, регулирует можно вызвать логику приложения или нет
type Throttled func(context.Context, string) (bool, string, error)

// * структура запоминает информацию о последнем запросе
type bucket struct {
	tokens uint
	time   time.Time
}

// * max    - макс количество токенов,
// * refill - на сколько токенов пополнять корзину клиента,
// * d      - с каким интервалом пополнять
func Throttle(e Effector, max uint, refill uint, d time.Duration) Throttled {
	//* мапка хранит информацию о последнем запросе каждого клиента
	buckets := map[string]*bucket{}

	//*вызывается в обработчике, обрабатывает запрос клиента
	return func(ctx context.Context, uid string) (bool, string, error) {
		b := buckets[uid]

		//* новый клиент
		if b == nil {
			//* добавляем его в мапку и отнимаем токен
			buckets[uid] = &bucket{tokens: max - 1, time: time.Now()}

			//* вызываем локику приложения (отдаем хостнейм)
			str, err := e(ctx)
			return true, str, err
		}

		//* клиент уже был
		//* перед тем как определить вызывать логику приложения, нужно пополнить корзину с токенами клиента
		//* учитывая время, прошедшее с момента последнего запроса
		refillInterval := uint(time.Since(b.time))
		tokensAdded := refill * refillInterval
		currentTokens := b.tokens + tokensAdded

		fmt.Printf("\n old client. \n\t tokens: %v   \n\t time: %v   \n\t time since %v \n\t tokens added: %v \n\t current tokens: %v \n",
			b.tokens,
			b.time,
			time.Since(b.time),
			tokensAdded,
			currentTokens,
		)

		//* отклоняю запрос клиента, если токенов недостаточно
		if currentTokens < 1 {
			return false, "", nil
		}

		//* если корзина пополнилась, запоминаем текущее время отнимаем токен от макс количества и вызываем локику приложения
		//* иначе определяем
		//* когда в последний раз добавлялись токены
		//* и отнимаем 1 токен от текущего количества токенов и вызываем локику приложения
		if currentTokens > max {
			b.time = time.Now()
			b.tokens = max - 1
		} else {
			deltaTokens := currentTokens - b.tokens
			deltaRefills := deltaTokens / refill
			deltaTime := time.Duration(deltaRefills) * d

			b.time = b.time.Add(deltaTime)
			b.tokens = currentTokens - 1
		}
		str, err := e(ctx)
		return true, str, err
	}
}
