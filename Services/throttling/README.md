# Throttlig

## Содержание

- [Описание](#about)
- [Начало работы](#getting_started)
- [Использование](#usage)

## Описание <a name = "about"></a>

Пример простого приложения, реализующий стратегию дроссельной заслонки для предотвращения перегрузки и каскадных сбоев.
- доступ к этому приложению можно получить через интерфейс REST :8080 GET /hostname 

## Начало работы <a name = "getting_started"></a>

- [Ссылка на телеграм канал](https://t.me/ITLodgeBack/6)
- [Ссылка на подробный дайджест](https://telegra.ph/Kaskadnye-sboi-Predotvrashchenie-peregruzki-04-17)



## Использование <a name = "usage"></a>

### запуск сервера
```
go mod tidy
go run main.go

```
### использование REST

```
curl localhost:8080/hostname
```
Для более глубокого понимания:
```
#!/bin/bash
for ((i = 0; i < 100000; i++)); do
  curl localhost:8080/hostname
done
```
